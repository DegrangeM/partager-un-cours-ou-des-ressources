# Partager un cours

_Ce tutoriel est placé sous licence CC-BY._

Vous souhaitez partager un cours sur la forge ? Alors ce tutoriel est pour vous.

Attention toutefois, n'oubliez pas que vous n'avez pas le droit de partager du contenu sous droit d'auteur sans autorisation. Partagez donc uniquement du contenu que vous avez créé ou bien dont vous avez obtenu les droits (en respectant les éventuelles contraintes associées).

## Création du projet/dépot
La première étape consiste à créer un projet/dépot.

- Une fois connecté à la forge, il suffit de cliquer sur le `+` en haut à gauche et de choisir `Nouveau projet/dépôt`.

- Sélectionnez ensuite `Créer un projet vide`, choisissez un nom pour votre projet, et réglez le niveau de visibilité sur `Public`. 

- Appuyez ensuite sur le bouton `Créer le projet`.

## Transfert des fichiers
Vous voici désormais sur votre projet (qui ne contient pour l'instant qu'un fichier `README.md` que vous pouvez ignorer pour l'instant).

La deuxième étape consiste à transférer vos fichiers de cours sur la plateforme.

Pour cela, trois méthodes existent, chacune ayant des avantages et des inconvéniants. Choisissez celle que vous préférez et cliquez pour obtenir les détails.

<details><summary>Fichier par fichier (simple, adapté à un nombre faible de fichiers)</summary>
Cette méthode, bien que lente puisque nécessitant d'ajouter chaque fichier un par un, est très simple.

Il vous suffit simplement de cliquer sur le bouton `+` à côté du nom de votre projet et de choisir `Téléverser un fichier`. 

![image](/uploads/cebd50515a5c36653676f62c93634497/image.png)

Choisissez votre fichier, appuyez sur le bouton `Téléverser un fichier` et répétez l'opération pour chacun des fichiers que vous souhaitez partager. A noter que le bouton `+` vous permet également de créer des dossiers si besoin.

</details>

<details><summary>Via WEB EDI (moyen, adapté aux néophytes qui ne veulent pas se prendre la tête)</summary>
Cliquez sur le bouton `Modifier` puis choisir `Web IDE`.

![image.png](/uploads/daecf10e98e2bd570facd0e9646a7247/image.png)

Cela va ouvrir une interface qui va vous permettre de mettre en ligne rapidement plusieurs fichiers. Ne vous inquiétez pas si l'interface vous fait peur, nous n'en utiliserons qu'une petite partie et vous serez guidé.

Dans le menu de gauche vous devriez retrouver les fichiers présents dans votre projet (uniquement le fichier README.md pour l'instant).

Vous pouvez maintenant glisser-déposer les fichiers de votre choix dans la zone à gauche (là où il y a le fichier README.md). Vous pouvez le faire en plusieurs fois et vous pouvez même glisser-déposer des dossiers entiers.

A noter que pour l'instant ces fichiers ne sont pas encore ajoutés à notre projet, il sont seulement chargée sur l'interface (qui est en réalité un éditeur avancé de fichiers textes). Il va falloir maintenant envoyer ces fichiers dans notre projet.

Pour cela, cliquez sur l'onglet `Source Control` puis sur `Commit`.

![image](/uploads/593ad6bb0e9c0f8be0f827c043295c20/image.png)

Si le message `You're committing your to the default branch. Do you want to continue ?` s'affiche, cliquez sur le bouton `Continue`.

Ca y-est, vos fichiers sont désormais en ligne sur votre projet et vous pouvez fermer l'application.

</details>

<details><summary>Via Git (complexe mais efficace)</summary>
La troisième méthode consiste à utiliser Git. Cette méthode est bien plus complexe et demande d'installer un ou plusieurs logiciels sur votre ordinateur. En revanche elle est très efficace pour si vous souhaitez partager régulièrement votre travail et exploiter au maximum les possibilités offertes par la plateforme.

TODO : faire un lien vers un tuto.
</details>

## Rédaction d'une présentation

Il peut être utile de présenter votre projet aux utilisateurs.

Pour cela rien de plus simple : parmi les fichiers de votre cours présent dans votre projet se trouve un fichier `README.md`.

Cliquez dessus pour l'ouvrir.

<details><summary>Afficher l'image d'illustration</summary>
![image](/uploads/00917d15b1fa93acc82434320fdde261/image.png)
</details>

Cliquez ensuite sur `Modifier` puis sur `Modifier le fichier unique`.

<details><summary>Afficher l'image d'illustration</summary>
![image](/uploads/225d3af54eac968c1a5747bad603921a/image.png)
</details>

Supprimez le contenu actuel du fichier et rédigez la présentation de votre projet.

N'oubliez pas de valider ensuite les modifications grâce en bouton en bas de page.


## Choix d'une licence

Puisque vous souhaitez partager votre cours, vous allez devoir choisir une licence pour votre contenu.

Une fois la licence choisie, indiquez-la dans la description de votre projet.

Différentes licences s'offrent à vous en fonction de vos préférences, vous pouvez par exemple mettre votre travail sous la licence `CC-BY` si vous souhaitez permettre à tout le monde d'utiliser votre travail à condition qu'ils vous citent comme auteur. Vous pouvez aussi choisir la licence `CC-BY-NC` pour permettre à tout le monde d'utiliser votre travail à condition qu'ils vous citent comme auteur et n'en face pas un usage commercial. Vous avez également la licence `CC-BY-ND` permet d'autoriser les autres à utiliser votre travail à condition de vous citer comme auteur et à condition de ne pas modifier votre travail, ainsi que la licence `CC-BY-SA` qui permet à tous le monde d'utiliser votre travail à condition de vous citer et de conserver la licence `CC-BY-SA` en cas de modification de votre travail. Il existe d'autres combinaisons, pour plus d'informations sur les licences Creatives Commons (mais il en existe également d'autres), n'hésitez pas à consulter le tableau ci-dessous.

<details><summary>Afficher le tableau d'aide au choix d'une licence</summary>

![image](https://drane-lyon.forge.aeif.fr/rel/Ressources/REL_Choose_Licence_CC_fr.png)

</details>

## Choix d'accepter ou non des contributions
todo

## Mettre à jour le dépot
todo
